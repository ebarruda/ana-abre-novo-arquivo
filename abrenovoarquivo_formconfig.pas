unit abrenovoarquivo_formconfig;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Spin, StdCtrls, Buttons;

type

  { TFormConfig }

  TFormConfig = class(TForm)
    BitBtnOk: TBitBtn;
    BitBtnCancelar: TBitBtn;
    ButtonSelecionar: TButton;
    CheckBoxAtivado: TCheckBox;
    ComboBoxPasta: TComboBox;
    ComboBoxExtensao: TComboBox;
    ComboBoxNomeclatura: TComboBox;
    Label10: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    SelectDirectoryDialogPasta: TSelectDirectoryDialog;
    SpinEditIntervaloTempo: TSpinEdit;
    procedure ButtonSelecionarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FormConfig: TFormConfig;

implementation

uses
  abrenovoarquivo_formprincipal;

{$R *.lfm}

{ TFormConfig }

procedure TFormConfig.FormCreate(Sender: TObject);
begin
  Left := (Screen.Width  div 2) - (Width div 2);
  Top  := (Screen.Height div 2) - (Height div 2);
end;

procedure TFormConfig.FormShow(Sender: TObject);
begin
  CheckBoxAtivado.Checked      := FormPrincipal.Ativado;
  SpinEditIntervaloTempo.Value := FormPrincipal.IntervaloTempo;
  ComboBoxExtensao.Text        := FormPrincipal.Extensao;
  ComboBoxPasta.Text           := FormPrincipal.Pasta;
  ComboBoxNomeclatura.Text     := FormPrincipal.StringListNomeclatura.DelimitedText;
end;

procedure TFormConfig.ButtonSelecionarClick(Sender: TObject);
begin
  SelectDirectoryDialogPasta.InitialDir := ComboBoxPasta.Text;

  if SelectDirectoryDialogPasta.Execute then begin
    ComboBoxPasta.Text := SelectDirectoryDialogPasta.FileName;
  end;
end;

end.

