program abrenovoarquivo;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, abrenovoarquivo_formprincipal, abrenovoarquivo_formconfig
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='ANA - Abre Novo Arquivo';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.CreateForm(TFormConfig, FormConfig);
  Application.Run;
end.

