unit abrenovoarquivo_formprincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  LCLProc, LCLIntf, Menus;

type

  { TFormPrincipal }

  TFormPrincipal = class(TForm)
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItemSobre: TMenuItem;
    MenuItemConfig: TMenuItem;
    MenuItemSair: TMenuItem;
    PopupMenuAbreNovoArquivo: TPopupMenu;
    TimerVerificaArquivo: TTimer;
    TrayIconAbreNovoArquivo: TTrayIcon;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuItemConfigClick(Sender: TObject);
    procedure MenuItemSairClick(Sender: TObject);
    procedure MenuItemSobreClick(Sender: TObject);
    procedure TimerVerificaArquivoTimer(Sender: TObject);
    procedure TrayIconAbreNovoArquivoDblClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    Ativado: Boolean;
    NaCriacao: Boolean;
    Configurando: Boolean;
    IntervaloTempo: Integer;
    Extensao: String;
    Pasta: String;
    StringListArquivo: TStringList;
    StringListNomeclatura: TStringList;
    procedure Configurar;
    procedure CriaListaArquivos;
    function MyGetPart(GTag, GRegistro: String): String;
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

uses
  abrenovoarquivo_formconfig;

{$R *.lfm}

{ TFormPrincipal }

procedure TFormPrincipal.CriaListaArquivos;
var
  XmlSearchRec: TSearchRec;
begin
  StringListArquivo.Clear;

  if FindFirst(AnsiToUtf8(Pasta) + '*.' + Extensao, faAnyFile, XmlSearchRec) = 0 then begin
    repeat
        StringListArquivo.Add(AnsiToUtf8(XmlSearchRec.Name));
    until FindNext(XmlSearchRec) <> 0;

    SysUtils.FindClose(XmlSearchRec);
  end;
end;


function TFormPrincipal.MyGetPart(GTag, GRegistro: String): String;
begin
  try
    Result := GetPart(['<' + LowerCase(GTag) + '>'], ['</' + LowerCase(GTag) + '>'], GRegistro, False, False);
  except
    Result := '';
  end;
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
  NaCriacao := true;

  StringListArquivo     := TStringList.Create;
  StringListNomeclatura := TStringList.Create;
end;

procedure TFormPrincipal.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  StringListArquivo.Free;
  StringListNomeclatura.Free;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
var
  Nomeclatura: String;
  StringListXml: TStringList;
begin
  if NaCriacao then begin
    NaCriacao                   := false;
    FormPrincipal.Visible       := false;
    FormPrincipal.ShowInTaskBar := stNever;

    StringListXml := TStringList.Create;

    with StringListXml do begin
      Add('<?xml version="1.0" encoding="UTF-8" ?>');
      Add('<arquivo_tipo>configuracao</arquivo_tipo>');
      Add('<nomeclatura>DocumentoDigitalizado</nomeclatura>');
      Add('<pasta>C:\temp</pasta>');
      Add('<extensao>pdf</extensao>');
      Add('<ativado>1</ativado>');
      Add('<intervalotempo>10</intervalotempo>');
      Add('<!-- F I M -->');

      try
        LoadFromFile('abrenovoarquivo.xml');
      except
      end;

      Pasta          := MyGetPart('pasta', Text);
      Nomeclatura    := MyGetPart('nomeclatura', Text);
      Extensao       := LowerCase(MyGetPart('extensao', Text));
      Ativado        := StrToBoolDef(MyGetPart('ativado', Text), true);
      IntervaloTempo := StrToIntDef(MyGetPart('intervalotempo', Text), 0);

      Extensao := StringReplace(Extensao, '.','', [rfReplaceAll, rfIgnoreCase]);
      Extensao := StringReplace(Extensao, '*','', [rfReplaceAll, rfIgnoreCase]);

      if Length(Trim(Pasta)) > 0 then begin
        if Pasta[Length(Pasta)] <> DirectorySeparator then begin
          Pasta := Pasta + DirectorySeparator;
        end;
      end;
    end;

    StringListXml.Free;

    StringListNomeclatura.Delimiter     := ',';
    StringListNomeclatura.DelimitedText := StringReplace(Nomeclatura, ';',',', [rfReplaceAll, rfIgnoreCase]);

    CriaListaArquivos;
  end;
end;

procedure TFormPrincipal.Configurar;
var
  Nomeclatura: String;
  StringListXml: TStringList;
begin
  Configurando := true;

  if FormConfig.ShowModal = mrOK then begin
    Ativado        := FormConfig.CheckBoxAtivado.Checked;
    IntervaloTempo := FormConfig.SpinEditIntervaloTempo.Value;
    Extensao       := LowerCase(FormConfig.ComboBoxExtensao.Text);
    Pasta          := FormConfig.ComboBoxPasta.Text;
    Nomeclatura    := FormConfig.ComboBoxNomeclatura.Text;

    Extensao := StringReplace(Extensao, '.','', [rfReplaceAll, rfIgnoreCase]);
    Extensao := StringReplace(Extensao, '*','', [rfReplaceAll, rfIgnoreCase]);

    if Length(Trim(Pasta)) > 0 then begin
      if Pasta[Length(Pasta)] <> DirectorySeparator then begin
        Pasta := Pasta + DirectorySeparator;
      end;
    end;

    StringListXml := TStringList.Create;

    with StringListXml do begin
      Add('<?xml version="1.0" encoding="UTF-8" ?>');
      Add('<arquivo_tipo>configuracao</arquivo_tipo>');
      Add('<pasta>' + Pasta + '</pasta>');
      Add('<nomeclatura>' + Nomeclatura + '</nomeclatura>');
      Add('<extensao>' + Extensao + '</extensao>');
      Add('<ativado>' + BoolToStr(Ativado, '1', '0') + '</ativado>');
      Add('<intervalotempo>' + IntToStr(IntervaloTempo) + '</intervalotempo>');
      Add('<!-- F I M -->');

      try
        SaveToFile('abrenovoarquivo.xml');
      except
      end;
    end;

    StringListXml.Free;

    StringListNomeclatura.DelimitedText := StringReplace(Nomeclatura, ';', ',', [rfReplaceAll, rfIgnoreCase]);

    CriaListaArquivos;
  end;

  Configurando := false;
end;


procedure TFormPrincipal.MenuItemConfigClick(Sender: TObject);
begin
  Configurar;
end;

procedure TFormPrincipal.TrayIconAbreNovoArquivoDblClick(Sender: TObject);
begin
  Configurar;
end;

procedure TFormPrincipal.MenuItemSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFormPrincipal.MenuItemSobreClick(Sender: TObject);
begin
  ShowMessage('Por Ericson Benjamim.');
end;

procedure TFormPrincipal.TimerVerificaArquivoTimer(Sender: TObject);
var
  Indice: Integer;
  IndiceNomeclatura: Integer;
  Nomeclatura: String;
  ArquivoNovo: String;
  XmlSearchRec: TSearchRec;
  FileStreamAN: TFileStream;
  StringListArquivoNovo: TStringList;
begin
  if Ativado and not Configurando then begin
    StringListArquivoNovo := TStringList.Create;

    if FindFirst(AnsiToUtf8(Pasta) + '*.' + Extensao, faAnyFile, XmlSearchRec) = 0 then begin
      repeat
          StringListArquivoNovo.Add(AnsiToUtf8(XmlSearchRec.Name));
      until FindNext(XmlSearchRec) <> 0;

      SysUtils.FindClose(XmlSearchRec);
    end;

    if StringListArquivoNovo.Count > 0 then begin
      for Indice := 0 to StringListArquivoNovo.Count - 1 do begin
        for IndiceNomeclatura := 0 to StringListNomeclatura.Count - 1 do begin
          Nomeclatura := StringListNomeclatura.Strings[IndiceNomeclatura];
        if UTF8Pos(LowerCase(Nomeclatura), LowerCase(StringListArquivoNovo.Strings[Indice])) > 0 then begin;
          if (StringListArquivo.Count > 0) then begin
            if StringListArquivo.IndexOf(StringListArquivoNovo.Strings[Indice]) < 0 then begin
              try
                FileStreamAN := TFileStream.Create(Pasta + StringListArquivoNovo.Strings[Indice], fmOpenReadWrite, fmShareExclusive);

                StringListArquivo.Add(StringListArquivoNovo.Strings[Indice]);
              except
              end;

              FreeAndNil(FileStreamAN);

              try
                OpenDocument(Pasta + StringListArquivoNovo.Strings[Indice]);
              except
              end;

              TrayIconAbreNovoArquivo.BalloonHint := 'Abrindo arquivo ' + Pasta + StringListArquivoNovo.Strings[Indice] + '...';
              TrayIconAbreNovoArquivo.ShowBalloonHint;
            end;
          end else begin
            try
              FileStreamAN := TFileStream.Create(Pasta + StringListArquivoNovo.Strings[Indice], fmOpenReadWrite, fmShareExclusive);

              StringListArquivo.Add(StringListArquivoNovo.Strings[Indice]);
            except
            end;

            FreeAndNil(FileStreamAN);

            try
              OpenDocument(Pasta + StringListArquivoNovo.Strings[Indice]);
            except
            end;

            TrayIconAbreNovoArquivo.BalloonHint := 'Abrindo arquivo ' + Pasta + StringListArquivoNovo.Strings[Indice] + '...';
            TrayIconAbreNovoArquivo.ShowBalloonHint;
          end;
        end;
        end;
      end;
    end;

    StringListArquivoNovo.Free;
  end;
end;

end.

